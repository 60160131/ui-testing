***Settings***
Library    SeleniumLibrary

***Variables***


***Test Cases***
คุณวรวิทย์ค้นหางาน Programmer และเพศไม่ตรงตามที่กำหนด
    เข้าเว็บไปที่หน้าค้นหา
    พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา
    หน้าเว็บจะขึ้นอาชีพ Programmer 4 ตัว
    เลือกตำแหน่งงาน Junior Node Programmer
    กดสมัคร
    ระบบแสดงผลลัพธ์ว่า เพศไม่ตรงตามที่กำหนด

***Keywords***
เข้าเว็บไปที่หน้าค้นหา
    Open Browser    http://localhost:3000/searchJob    chrome
    Set Selenium Speed    0.5

พิมพ์คำค้นหาว่า "Programmer" และกดปุ่มค้นหา
    Input Text    id=search_text    Programmer
    Click Element    id=search_button

หน้าเว็บจะขึ้นอาชีพ Programmer 4 ตัว
    Element Text Should Be    id=name_4    Junior Node Programmer

เลือกตำแหน่งงาน Junior Node Programmer
    Click Element    id=show_detail_4
    Wait Until Element Contains    id=name    Junior Node Programmer

กดสมัคร
    Click Element    id=apply_job

ระบบแสดงผลลัพธ์ว่า เพศไม่ตรงตามที่กำหนด
    Element Should Contain    id=message    Gender does not meet the conditions
